from rest_framework.views import APIView
from django.conf import settings
import pickle
from apiclient.discovery import build
from django.http import JsonResponse

# Create your views here.


class CommentVideo(APIView):
    def get(self, request):
        video_id = request.GET.get('video_id')
        comment = request.GET.get('comment')
        if video_id is not None:
            if comment is not None:
                credential = pickle.load(open("token.pkl", "rb"))
                youtube = build('youtube', 'v3', developerKey=settings.YOUTUBE_DATA_API_KEY, credentials=credential)
                y = youtube.commentThreads().insert(
                        part="snippet",
                        body={
                            "snippet": {
                                "videoId": video_id,
                                "topLevelComment": {
                                    "snippet": {
                                        "textOriginal": comment
                                    }
                                }
                            }
                        }
                    ).execute()
                if y is not None:
                    return JsonResponse({'msg': 'Done'})
                else:
                    return JsonResponse({'error': 'Got Error'})
            else:
                return JsonResponse({'error': 'Please provide comment'})
        else:
            return JsonResponse({'error': 'Please provide video id'})
