from django.urls import path
from .views import CommentVideo
urlpatterns = [
    path('', CommentVideo.as_view(), name='comment-video')
]
