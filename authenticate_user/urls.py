from django.urls import path
from .views import Authentication, GetToken
urlpatterns = [
   path('url', Authentication.as_view(), name='authentication'),
   path('get-token/', GetToken.as_view(), name='get-token')
]
