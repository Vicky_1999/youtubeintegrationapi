from django.http import JsonResponse
from rest_framework.views import APIView
import google_auth_oauthlib
from google_auth_oauthlib.flow import InstalledAppFlow
import pickle


class Authentication(APIView):
    def post(self, request):
        flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
            'client_secret.json',
            scopes=['https://www.googleapis.com/auth/youtube.force-ssl'])
        flow.redirect_uri = 'http://127.0.0.1:8000/search_youtube'
        authorization_url, state = flow.authorization_url(access_type='offline', include_granted_scopes='true')
        return JsonResponse(authorization_url, safe=False)


class GetToken(APIView):
    def get(self, request):
        state = request.GET['state']
        flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
            'client_secret.json',
            scopes=['https://www.googleapis.com/auth/youtube.force-ssl'],
            state=state)
        flow.redirect_uri = 'http://127.0.0.1:8000/search_youtube'
        flow.fetch_token(code=request.GET.get('code'))
        credentials = flow.credentials
        pickle.dump(credentials, open("token.pkl", "wb"))
        return JsonResponse({'msg': "token Successfully created, named as 'token.pkl'"})


