from rest_framework.views import APIView
from django.conf import settings
import pickle
from apiclient.discovery import build
from django.http import JsonResponse
# Create your views here.


class Rate(APIView):
    def get(self, request):
        rate = request.GET.get('rate')
        if rate is None:
            rate = 'none'
        video_id = request.GET.get('id_')
        if video_id is not None:
            credential = pickle.load(open("token.pkl", "rb"))
            youtube = build('youtube', 'v3', developerKey=settings.YOUTUBE_DATA_API_KEY, credentials=credential)
            youtube.videos().rate(rating=rate, id=str(video_id)).execute()
            return JsonResponse({'msg': rate+' it', 'Video': 'https://www.youtube.com/watch?v='+video_id})
        else:
            return JsonResponse({'error': 'Please provide video_id'})
