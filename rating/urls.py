from django.urls import path
from .views import Rate
urlpatterns = [
    path('', Rate.as_view(), name='rate')
]
