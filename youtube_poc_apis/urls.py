from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('authenticate/', include('authenticate_user.urls')),
    path('rating/', include('rating.urls')),
    path('comment', include('comment.urls'))
]
